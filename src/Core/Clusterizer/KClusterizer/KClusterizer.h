/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "../Clusterizer.h"
#include <Utils/PriorityBuffer.h>

namespace PTracking
{
	/**
	 * @class KClusterizer
	 * 
	 * @brief Class that implements a clustering algorithm called KClusterizer.
	 * 
	 * KClusterizer does not require any initialization, it is very fast and all the obtained clusters have a specified maximum radius given as input. Indeed, the
	 * desired number of clusters \a k is initially set to 1 and dynamically adapted by the architecture, in which the KClusterizer algorithm is used, in order to
	 * reflect the number of objects to be tracked.
	 */
	class KClusterizer : public Clusterizer
	{
		private:
			/**
			 * @struct Cluster
			 * 
			 * @brief Struct that represents a cluster.
			 */
			struct Cluster
			{
				/**
				 * @brief centroid of the cluster.
				 */
				PoseParticle k;
				
				/**
				 * @brief particles contained in the cluster.
				 */
				std::vector<int> indices;
			};
			
			/**
			 * @brief The minimum number of particles inside a splitted cluster needed to actually become a cluster.
			 */
			static const unsigned int MIN_SIZE_CLUSTER_AFTER_SPLITTING = 15;
			
			/**
			 * @brief spaced out particles that represent the centroids of the clusters.
			 */
			PriorityBuffer<PoseParticle> kpoints;
			
			/**
			 * @brief value of the current number of clusters \a k used during the clustering phase.
			 */
			int maxClusters;
			
			/**
			 * @brief Function that checks if two particles are far each other within a maximum threshold distance.
			 * 
			 * @param p1 reference to the first particle to be checked.
			 * @param p2 reference to the second particle to be checked.
			 * @param qualityThreshold maximum threshold distance.
			 * 
			 * @return \b true if the particles are far each other, \b false otherwise.
			 */
			bool isFarFrom(const PoseParticle& p1, const PoseParticle& p2, float qualityThreshold) const;
			
			/**
			 * @brief Function that checks if the particle is far from all the others within a maximum threshold distance.
			 * 
			 * @param p reference to the particle to be checked.
			 * @param qualityThreshold maximum threshold distance.
			 * 
			 * @return \b true if the particle is far from all the others, \b false otherwise.
			 */
			bool isFarFromAll(const PoseParticle& p, float qualityThreshold) const;
			
		public:
			/**
			 * @brief Constructor that takes the desired maximum number of clusters as initialization value.
			 * 
			 * It initializes the desired number of clusters with the one given as input.
			 * 
			 * @param maxClusters desired maximum number of clusters.
			 */
			KClusterizer(int maxClusters);
			
			/**
			 * @brief Destructor.
			 */
			~KClusterizer();
			
			/**
			 * @brief Function that clusterizes a vector of particles creating a set of clusters with a maximum radius given as input.
			 * 
			 * @param particleVector reference to the particles' vector that have to be clusterized.
			 * @param qualityThreshold maximum radius of a cluster.
			 */
			void clusterize(const PoseParticleVector& particleVector, float qualityThreshold);
			
			/**
			 * @brief Function that returns the number of clusters \a k used by the clustering algorithm to clusterize the particles.
			 * 
			 * @return the desired number of clusters \a k.
			 */
			int getCurrentClusterNumber() const { return maxClusters; }
			
			/**
			 * @brief Function that updates the value of \a k representing the desired number of clusters.
			 * 
			 * @param k new value of the desired clusters.
			 */
			void setMaxClusterNumber(int k);
	};
}
