/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "ObjectParticleFilterMultiAgent.h"
#include "../Clusterizer/KClusterizer/KClusterizer.h"
#include "../Clusterizer/QTClusterizer/QTClusterizer.h"
#include "../SensorMaps/BasicSensorMap.h"
#include "../Sensors/BasicSensor.h"
#include "../../Utils/Timestamp.h"
#include <Manfield/Utils/DebugUtils.h>
#include <Manfield/ConfigFile/ConfigFile.h>
#include <fstream>

using namespace std;
using namespace cv;
using GMapping::ConfigFile;
using GMapping::SensorReading;
using manfield::PrototypeFactory;
using manfield::SensorModel;

namespace PTracking
{
ObjectParticleFilterMultiAgent::ObjectParticleFilterMultiAgent(const string& type) : ParticleFilter(type), R(2,2,DataType<float>::type), maxIdentityNumber(0) {;}
	
	ObjectParticleFilterMultiAgent::~ObjectParticleFilterMultiAgent() {;}
	
	PoseParticle ObjectParticleFilterMultiAgent::adjustWeight(const PoseParticle& p, unsigned long particlesTimestamp, unsigned long currentTimestamp, Utils::DecreaseModelFactor model, float factor) const
	{
		PoseParticle g;
		
		if (model == Utils::Linear)
		{
			g.pose = p.pose;
			
			/// The timestamp is in milliseconds that is why we divide by 1000.0.
			g.weight -= (factor * (((float) (currentTimestamp - particlesTimestamp)) / 1000.0));
			
			if (g.weight < 0.0) g.weight = 0.0;
		}
		
		return g;
	}
	
	void ObjectParticleFilterMultiAgent::configure(const string& filename, int port, float frameRate, const string& calibrationDirectory, int agentId)
	{
		ConfigFile fCfg;
		stringstream s;
		string key, section, temp;
		float worldXMin = 0, worldXMax = 0, worldYMin = 0, worldYMax = 0;
		bool calibrateData;
		
		agentPort = port;
		
		if (!fCfg.read(filename))
		{
			ERR("Error reading file '" << filename << "' for filter configuration. Exiting..." << endl);
			
			exit(-1);
		}
		
		ParticleFilter::configure(filename);
		
		try
		{
			section = "parameters";
			
			key = "bestParticles";
			bestParticlesNumber = fCfg.value(section,key);
			
			key = "distributedClosenessThreshold";
			distributedClosenessThreshold = fCfg.value(section,key);
			
			key = "calibrateData";
			calibrateData = fCfg.value(section,key);
			
			key = "frameToWaitBeforeDeleting";
			frameToWaitBeforeDeleting = fCfg.value(section,key);
			
			/// Number of frames times the desired frame rate.
			timeToWaitBeforeDeleting = frameToWaitBeforeDeleting * (1000.0 / frameRate);
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		string filterName = getname();
		
		SensorModel* sm = 0;
		
		try
		{
			section = "sensormodel";
			key = "type";
			
			sm = PrototypeFactory<SensorModel>::forName(fCfg.value(section,key));
			
			if (sm != 0)
			{
				m_sensorModel = sm;
			}
			else
			{
				ERR("Missing prototype for sensorModel " << string(fCfg.value(section,key)) << ". Exiting..." << endl);
				
				exit(-1);
			}
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		m_sensorModel->configure(filename,new BasicSensor(filterName));
		
		BasicSensorMap* basicSensorMap = new BasicSensorMap();
		
		/// Reading sensor locations.
		if (calibrateData) section = "location-calibrated";
		else section = "location";
		
		try
		{
			key = "worldXMin";
			temp = string(fCfg.value(section,key));
			
			if (temp == "-inf") worldXMin = -FLT_MAX;
			else worldXMin = atof(temp.c_str());
			
			key = "worldXMax";
			temp = string(fCfg.value(section,key));
			
			if (temp == "inf") worldXMax = FLT_MAX;
			else worldXMax = atof(temp.c_str());
			
			key = "worldYMin";
			temp = string(fCfg.value(section,key));
			
			if (temp == "-inf") worldYMin = -FLT_MAX;
			else worldYMin = atof(temp.c_str());
			
			key = "worldYMax";
			temp = string(fCfg.value(section,key));
			
			if (temp == "inf") worldYMax = FLT_MAX;
			else worldYMax = atof(temp.c_str());
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		basicSensorMap->setworldMin(Point2f(worldXMin,worldYMin));
		basicSensorMap->setworldMax(Point2f(worldXMax,worldYMax));
		
		/// Type of clustering.
		section = "clustering";
		
		try
		{
			key = "algorithm";
			clusteringAlgorithm = string(fCfg.value(section,key));
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
		
		if (calibrateData)
		{
			if (!fCfg.read(calibrationDirectory + string("/transformation.cfg")))
			{
				ERR("Error reading file '" << string(calibrationDirectory + string("/transformation.cfg")) << "' for PTaLer configuration. Exiting..." << endl);
			
				exit(-1);
			}
			
			try
			{
				if (calibrationDirectory.at(calibrationDirectory.size() - 1) == '/') temp = calibrationDirectory.substr(0,calibrationDirectory.size() - 1);
				else temp = calibrationDirectory;
				
				s << temp.substr(temp.rfind("/") + 1) << "_" << agentId;
				
				section = s.str() + string("-axes");
				
				key = "xFlipped";
				xFlipped = fCfg.value(section,key);
				
				key = "yFlipped";
				yFlipped = fCfg.value(section,key);
				
				section = s.str() + string("-rotation");
				
				key = "R00";
				R.at<float>(0,0) = fCfg.value(section,key);
				
				key = "R01";
				R.at<float>(0,1) = fCfg.value(section,key);
				
				key = "R10";
				R.at<float>(1,0) = fCfg.value(section,key);
				
				key = "R11";
				R.at<float>(1,1) = fCfg.value(section,key);
			}
			catch (...)
			{
				ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
				
				exit(-1);
			}
		}
		
		if (strcasecmp(clusteringAlgorithm.c_str(),"KClusterizer") == 0) clusterizer = new KClusterizer(1);
		else if (strcasecmp(clusteringAlgorithm.c_str(),"QTClusterizer") == 0) clusterizer = new QTClusterizer();
		
		Point2f p;
		ifstream ifs;
		string ifsFilename = filename, sensorName;
		char buf[256];
		
		ifs.open(ifsFilename.c_str());
		
		while (ifs.good())
		{
			if (ifs.eof()) break;
			
			ifs.getline(buf,256);
			
			if (string(buf).compare(0,filterName.length(),filterName) == 0)
			{
				istringstream iss(buf);
				
				iss >> sensorName >> p.x >> p.y;
				
				basicSensorMap->insertSensor(p);
			}
		}
		
		ifs.close();
		
		m_sensorModel->setSensorMap(basicSensorMap);
		
		if (calibrateData) distributedClosenessThreshold = min(0.8f,2 * distributedClosenessThreshold);
		else distributedClosenessThreshold = min(40.0f,2 * distributedClosenessThreshold);
		
		ERR("######################################################" << endl);
		DEBUG("Distributed particle filter parameters:" << endl);
		INFO("\tDistibuted data association threshold (in " << (calibrateData ? "meters" : "pixels") << "): " << distributedClosenessThreshold << endl);
		DEBUG("\tFrame to wait before deleting: " << frameToWaitBeforeDeleting << endl);
		ERR("######################################################" << endl << endl);
	}
	
	bool ObjectParticleFilterMultiAgent::isSameDirection(const pair<int,pair<ObjectSensorReading::Observation,Point2f> >& e1, const pair<int,pair<ObjectSensorReading::Observation,Point2f> >& e2) const
	{
		return (Utils::calculateEstimationDirection(e1.second.first.model.velocity) == Utils::calculateEstimationDirection(e2.second.first.model.velocity));
	}
	
	void ObjectParticleFilterMultiAgent::observe(const vector<ObjectSensorReadingMultiAgent>& readings)
	{
		PoseParticleVector fusedParticles;
		unsigned long timestampLastIteration;
		
		timestampLastIteration = currentTimestamp;
		currentTimestamp = Timestamp().getMsFromMidnight();
		
		for (vector<ObjectSensorReadingMultiAgent>::const_iterator it = readings.begin(); it != readings.end(); ++it)
		{
			const SingleAgentEstimations& estimationsWithtModels = it->getEstimationsWithModels();
			
			for (SingleAgentEstimations::const_iterator it2 = estimationsWithtModels.begin(); it2 != estimationsWithtModels.end(); ++it2)
			{
				const PoseParticleVector& part = Utils::samplingParticles(it2->second.first.observation,it2->second.first.sigma,bestParticlesNumber);
				
				for (PoseParticleVector::const_iterator it3 = part.begin(); it3 != part.end(); it3++)
				{
					fusedParticles.push_back(adjustWeight(*it3,it->getEstimationsTimestamp(),currentTimestamp,Utils::Linear,0.15));
				}
			}
		}
		
		m_params.m_particles = fusedParticles;
		
		for (PoseParticleVector::iterator i = m_params.m_particles.begin(); i != m_params.m_particles.end(); i++)
		{
			if (i->weight > m_params.m_maxParticle.weight)
			{
				m_params.m_maxParticle = *i;
			}
		}
		
		updateTargetIdentity(readings,timestampLastIteration);
		
		//clusterizer->clusterize(m_params.m_particles,distributedClosenessThreshold);
		//clusters = clusterizer->getClusters();
		
		resample();
	}
	
	bool ObjectParticleFilterMultiAgent::reset()
	{
		clusters.clear();
		estimatedTargetModelsWithIdentityMultiAgent.clear();
		estimationsMultiAgentUpdateTime.clear();
		
		maxIdentityNumber = 0;
		
		delete clusterizer;
		
		if (strcasecmp(clusteringAlgorithm.c_str(),"KClusterizer") == 0) clusterizer = new KClusterizer(1);
		else if (strcasecmp(clusteringAlgorithm.c_str(),"QTClusterizer") == 0) clusterizer = new QTClusterizer();
		
		return true;
	}
	
	void ObjectParticleFilterMultiAgent::resetWeight()
	{
		int i, size;
		
		i = 0;
		size = m_params.m_particles.size();
		
		/// Partial Loop Unrolling to better use pipeling.
		for (; i < size - 3; i += 4)
		{
			m_params.m_particles.at(i).weight = 1.0;
			m_params.m_particles.at(i + 1).weight = 1.0;
			m_params.m_particles.at(i + 2).weight = 1.0;
			m_params.m_particles.at(i + 3).weight = 1.0;
		}
		
		for (; i < size; ++i)
		{
			m_params.m_particles.at(i).weight = 1.0;
		}
	}
	
	void ObjectParticleFilterMultiAgent::updateTargetIdentity(const vector<ObjectSensorReadingMultiAgent>& readings, unsigned long timestampLastIteration)
	{
		vector<pair<ObjectSensorReading::Observation,Point2f> > estimationsToBeFused;
		float allSigmaX, allSigmaY, distance, globalEstimationX, globalEstimationY, globalEstimationHeadX, globalEstimationHeadY, minDistance, sigmaNormalizationRatioX, sigmaNormalizationRatioY;
		float globalAveragedVelocityX, globalAveragedVelocityY, globalVelocityX, globalVelocityY;
		int currentIndex;
		
		currentIndex = -1;
		
		/// Analyzing each agent.
		for (vector<ObjectSensorReadingMultiAgent>::const_iterator it = readings.begin(); it != readings.end(); ++it)
		{
			const SingleAgentEstimations& estimations = it->getEstimationsWithModels();
			
			/// Analyzing all the estimations performed by the agent.
			for (SingleAgentEstimations::const_iterator it2 = estimations.begin(); it2 != estimations.end(); ++it2)
			{
				estimationsToBeFused.clear();
				estimationsToBeFused.push_back(it2->second);
				
				vector<ObjectSensorReadingMultiAgent>::const_iterator it3;
				
				it3 = it;
				advance(it3,1);
				
				/// Analyzing all the other agents.
				for (; it3 != readings.end(); ++it3)
				{
					SingleAgentEstimations& estimationsOtherAgent = *const_cast<SingleAgentEstimations*>(&it3->getEstimationsWithModels());
					
					/// Analyzing all the estimations performed by the other agent.
					for (SingleAgentEstimations::iterator it4 = estimationsOtherAgent.begin(); it4 != estimationsOtherAgent.end(); )
					{
						//if (isSameDirection(*it2,*it4))
						{
							/// Keeping estimations having the same direction and that are sufficiently close each other.
							if (Utils::isTargetNear(it2->second.first.observation,it4->second.first.observation,distributedClosenessThreshold))
							{
								estimationsToBeFused.push_back(it4->second);
								estimationsOtherAgent.erase(it4++);
							}
							else ++it4;
						}
						//else ++it4;
					}
				}
				
				allSigmaX = 0.0;
				allSigmaY = 0.0;
				
				for (vector<pair<ObjectSensorReading::Observation,Point2f> >::const_iterator it4 = estimationsToBeFused.begin(); it4 != estimationsToBeFused.end(); ++it4)
				{
					allSigmaX += it4->second.x;
					allSigmaY += it4->second.y;
				}
				
				/// Fusing estimations.
				if (estimationsToBeFused.size() > 1)
				{
					globalEstimationX = 0.0;
					globalEstimationY = 0.0;
					
					globalEstimationHeadX = 0.0;
					globalEstimationHeadY = 0.0;
					
					sigmaNormalizationRatioX = 0.0;
					sigmaNormalizationRatioY = 0.0;
					
					for (vector<pair<ObjectSensorReading::Observation,Point2f> >::const_iterator it5 = estimationsToBeFused.begin(); it5 != estimationsToBeFused.end(); ++it5)
					{
						sigmaNormalizationRatioX += (1.0 - (it5->second.x / allSigmaX));
						sigmaNormalizationRatioY += (1.0 - (it5->second.y / allSigmaY));
					}
					
					sigmaNormalizationRatioX = 1.0 / sigmaNormalizationRatioX;
					sigmaNormalizationRatioY = 1.0 / sigmaNormalizationRatioY;
					
					for (vector<pair<ObjectSensorReading::Observation,Point2f> >::const_iterator it5 = estimationsToBeFused.begin(); it5 != estimationsToBeFused.end(); ++it5)
					{
						globalEstimationX += (it5->first.observation.x * ((1.0 - (it5->second.x / allSigmaX)) * sigmaNormalizationRatioX));
						globalEstimationY += (it5->first.observation.y * ((1.0 - (it5->second.y / allSigmaY)) * sigmaNormalizationRatioY));
						
						globalEstimationHeadX += (it5->first.head.x * ((1.0 - (it5->second.x / allSigmaX)) * sigmaNormalizationRatioX));
						globalEstimationHeadY += (it5->first.head.y * ((1.0 - (it5->second.y / allSigmaY)) * sigmaNormalizationRatioY));
					}
					
					globalVelocityX = (R.at<float>(0,0) * estimationsToBeFused.begin()->first.model.velocity.x + R.at<float>(0,1) * estimationsToBeFused.begin()->first.model.velocity.y);
					globalVelocityY = (R.at<float>(1,0) * estimationsToBeFused.begin()->first.model.velocity.x + R.at<float>(1,1) * estimationsToBeFused.begin()->first.model.velocity.y);
					
					if (xFlipped) globalVelocityX *= -1;
					if (yFlipped) globalVelocityY *= -1;
					
					globalAveragedVelocityX = (R.at<float>(0,0) * estimationsToBeFused.begin()->first.model.averagedVelocity.x +
											   R.at<float>(0,1) * estimationsToBeFused.begin()->first.model.averagedVelocity.y);
					
					globalAveragedVelocityY = (R.at<float>(1,0) * estimationsToBeFused.begin()->first.model.averagedVelocity.x +
											   R.at<float>(1,1) * estimationsToBeFused.begin()->first.model.averagedVelocity.y);
					
					if (xFlipped) globalAveragedVelocityX *= -1;
					if (yFlipped) globalAveragedVelocityY *= -1;
				}
				else
				{
					globalEstimationX = estimationsToBeFused.begin()->first.observation.x;
					globalEstimationY = estimationsToBeFused.begin()->first.observation.y;
					
					globalEstimationHeadX = estimationsToBeFused.begin()->first.head.x;
					globalEstimationHeadY = estimationsToBeFused.begin()->first.head.y;
					
					globalVelocityX = (R.at<float>(0,0) * estimationsToBeFused.begin()->first.model.velocity.x + R.at<float>(0,1) * estimationsToBeFused.begin()->first.model.velocity.y);
					globalVelocityY = (R.at<float>(1,0) * estimationsToBeFused.begin()->first.model.velocity.x + R.at<float>(1,1) * estimationsToBeFused.begin()->first.model.velocity.y);
					
					if (xFlipped) globalVelocityX *= -1;
					if (yFlipped) globalVelocityY *= -1;
					
					globalAveragedVelocityX = (R.at<float>(0,0) * estimationsToBeFused.begin()->first.model.averagedVelocity.x +
											   R.at<float>(0,1) * estimationsToBeFused.begin()->first.model.averagedVelocity.y);
					
					globalAveragedVelocityY = (R.at<float>(1,0) * estimationsToBeFused.begin()->first.model.averagedVelocity.x +
											   R.at<float>(1,1) * estimationsToBeFused.begin()->first.model.averagedVelocity.y);
					
					if (xFlipped) globalAveragedVelocityX *= -1;
					if (yFlipped) globalAveragedVelocityY *= -1;
				}
				
				ObjectSensorReading::Observation globalEstimation;
				
				globalEstimation.observation.x = globalEstimationX;
				globalEstimation.observation.y = globalEstimationY;
				globalEstimation.head.x = globalEstimationHeadX;
				globalEstimation.head.y = globalEstimationHeadY;
				globalEstimation.sigma = Point2f(allSigmaX / estimationsToBeFused.size(), allSigmaY / estimationsToBeFused.size());
				globalEstimation.model = it2->second.first.model;
				
				globalEstimation.model.velocity.x = globalVelocityX;
				globalEstimation.model.velocity.y = globalVelocityY;
				globalEstimation.model.averagedVelocity.x = globalAveragedVelocityX;
				globalEstimation.model.averagedVelocity.y = globalAveragedVelocityY;
				
				minDistance = FLT_MAX;
				
				/// Finding the current index of such an estimation, if any.
				for (MultiAgentEstimations::const_iterator it3 = estimatedTargetModelsWithIdentityMultiAgent.begin(); it3 != estimatedTargetModelsWithIdentityMultiAgent.end(); ++it3)
				{
					const Point2f& e = it3->second.first.first.observation;
					
					distance = sqrt(((globalEstimationX - e.x) * (globalEstimationX - e.x)) + ((globalEstimationY - e.y) * (globalEstimationY - e.y)));
					
					if (distance < minDistance)
					{
						minDistance = distance;
						currentIndex = it3->first;
					}
				}
				
				if (minDistance <= distributedClosenessThreshold)
				{
					const MultiAgentEstimations::iterator& estimation = estimatedTargetModelsWithIdentityMultiAgent.find(currentIndex);
					
					estimation->second.first.first = globalEstimation;
					estimation->second.first.second = globalEstimation.sigma;
					
					const map<int,Timestamp>::iterator& estimationTime = estimationsMultiAgentUpdateTime.find(currentIndex);
					
					/// Updating the timestamp of the estimation.
					estimationTime->second.setToNow();
				}
				else
				{
					int targetIdentity;
					bool reidentifiedEstimation;
					
					targetIdentity = -1;
					reidentifiedEstimation = false;
					
					/// Checking if	the local identity of the object can be used. At this moment, I do not care about which agent provides the identity of the object, that is why I am using the first one.
					const MultiAgentEstimations::iterator& estimation = estimatedTargetModelsWithIdentityMultiAgent.find(it2->first);
					
					if (estimation == estimatedTargetModelsWithIdentityMultiAgent.end()) targetIdentity = it2->first;
					else
					{
						/// A same identity has been assigned by two different agents to a two different objects.
						if (agentPort != it->getAgent().second) targetIdentity = ++maxIdentityNumber;
						else
						{
							const map<int,Timestamp>::iterator& estimationTime = estimationsMultiAgentUpdateTime.find(it2->first);
							
							/// It means that the current estimation is provided by the same agent. It has been lost and recovered by the same agent in the local frame, and in the global frame it is still alive (deleting timeout not reached).
							if (estimationTime->second.getMs() > (1.5 * (Timestamp().getMsFromMidnight() - timestampLastIteration)))
							{
								estimation->second.first.first = globalEstimation;
								estimation->second.first.second = globalEstimation.sigma;
								
								/// Updating the timestamp of the estimation.
								estimationTime->second.setToNow();
								
								reidentifiedEstimation = true;
							}
							else targetIdentity = ++maxIdentityNumber;
						}
					}
					
					if (!reidentifiedEstimation)
					{
						estimatedTargetModelsWithIdentityMultiAgent.insert(make_pair(targetIdentity,make_pair(make_pair(globalEstimation,globalEstimation.sigma),it->getAgent())));
						estimationsMultiAgentUpdateTime.insert(make_pair(targetIdentity,Timestamp()));
					}
				}
			}
		}
		
		/// Removing all the estimations that have not been associated for a period equals to timeToWaitBeforeDeleting.
		for (MultiAgentEstimations::iterator it = estimatedTargetModelsWithIdentityMultiAgent.begin(); it != estimatedTargetModelsWithIdentityMultiAgent.end(); )
		{
			const map<int,Timestamp>::iterator& estimationTime = estimationsMultiAgentUpdateTime.find(it->first);
			
			if (((long) currentTimestamp - (long) estimationTime->second.getMsFromMidnight()) > (long) timeToWaitBeforeDeleting)
			{
				estimatedTargetModelsWithIdentityMultiAgent.erase(it++);
				estimationsMultiAgentUpdateTime.erase(estimationTime);
			}
			else ++it;
		}
		
		if (!estimatedTargetModelsWithIdentityMultiAgent.empty() && (estimatedTargetModelsWithIdentityMultiAgent.rbegin()->first > maxIdentityNumber))
		{
			maxIdentityNumber = estimatedTargetModelsWithIdentityMultiAgent.rbegin()->first;
		}
	}
	
	void ObjectParticleFilterMultiAgent::updateTimeToWaitBeforeDeleting(float frameRate)
	{
		/// Number of frames times the desired frame rate.
		timeToWaitBeforeDeleting = frameToWaitBeforeDeleting * (1000.0 / frameRate);
	}
	
	SENSORFILTER_FACTORY(ObjectParticleFilterMultiAgent)
}
