/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "ObjectSensorReading.h"
#include <Utils/Utils.h>

using namespace std;

namespace PTracking
{
	ObjectSensorReading::ObjectSensorReading() {;}
	
	ObjectSensorReading::~ObjectSensorReading() {;}
	
	void ObjectSensorReading::setObservations(const vector<Observation>& obs)
	{
		observations = obs;
	}
	
	void ObjectSensorReading::setObservations(Observation targetPoints[], int currentTargetIndex, int lastCurrentTargetIndex, int lastNTargetPerception, const Point2f& worldX, const Point2f& worldY)
	{
		float x, y;
		int i;
		
		observations.clear();
		
		i = lastCurrentTargetIndex;
		
		while (i != currentTargetIndex)
		{
			x = targetPoints[i].observation.x;
			y = targetPoints[i].observation.y;
			
			/// The x value represents the minimum world limit while the y value represents the maximum one.
			if (((x >= worldX.x) && (x <= worldX.y)) && ((y >= worldY.x) && (y <= worldY.y)))
			{
				targetPoints[i].observation.x = x;
				targetPoints[i].observation.y = y;
				
				observations.push_back(targetPoints[i]);
			}
			
			++i;
			
			/// To avoid an infinity loop.
			if (i == currentTargetIndex) break;
			
			/// Because targetPoints is a circular buffer.
			if (i == lastNTargetPerception) i = 0;
		}
	}
	
	void ObjectSensorReading::setObservationsAgentPose(const Point2of& pose)
	{
		observationsAgentPose = pose;
	}
	
	void ObjectSensorReading::setSensor(const BasicSensor& s)
	{
		sensor = s;
	}
}
