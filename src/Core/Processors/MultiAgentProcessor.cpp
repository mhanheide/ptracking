/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "MultiAgentProcessor.h"
#include "Core/Clusterizer/Clusterizer.h"
#include <Manfield/ConfigFile/ConfigFile.h>
#include <Manfield/Utils/DebugUtils.h>

using namespace std;
using GMapping::ConfigFile;
using GMapping::SensorReading;

namespace PTracking
{
	// In seconds.
	static const float UPDATE_FREQUENCY = 1;
	
	MultiAgentProcessor::MultiAgentProcessor() : ManifoldFilterProcessor(), m_updateFrequency(UPDATE_FREQUENCY), m_nFusedParticles(0) {;}
	
	MultiAgentProcessor::~MultiAgentProcessor() {;}
	
	void MultiAgentProcessor::init()
	{
		ManifoldFilterProcessor::init();
	}
	
	void MultiAgentProcessor::processReading(const vector<ObjectSensorReadingMultiAgent>& readings)
	{
		if (m_first)
		{
			m_first = false;
			m_bootstrapRequired = true;
		}
		
		if (updateNeeded())
		{
			for (FilterBank::const_iterator it = m_filterBank.begin(); it != m_filterBank.end(); it++)
			{
				singleFilterIteration(*static_cast<ObjectParticleFilterMultiAgent*>(it->second),readings);
			}
			
			timeOfLastIteration.setToNow();
		}
	}
	
	void MultiAgentProcessor::singleFilterIteration(ObjectParticleFilterMultiAgent& f, const vector<ObjectSensorReadingMultiAgent>& readings) const
	{
		f.observe(readings);
	}
	
	bool MultiAgentProcessor::updateNeeded() const
	{
		return true;
	}
}
