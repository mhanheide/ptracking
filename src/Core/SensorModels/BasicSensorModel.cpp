/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "BasicSensorModel.h"
#include <Manfield/ConfigFile/ConfigFile.h>
#include <Manfield/Utils/DebugUtils.h>
#include <stdlib.h>

using namespace std;
using namespace GMapping;

namespace PTracking
{
	BasicSensorModel::BasicSensorModel(const string& type) : SensorModel(type), linearVelocity(modelLinearVelocity) {;}
	
	BasicSensorModel::~BasicSensorModel() {;}
	
	void BasicSensorModel::calculateLinearVelocity(unsigned long initialTimestamp, unsigned long currentTimestamp)
	{
		linearVelocity = modelLinearVelocity * exp(-((float) (currentTimestamp - initialTimestamp)));
	}
	
	void BasicSensorModel::configure(const string& filename, GMapping::Sensor* sensor)
	{
		ConfigFile fCfg;
		string key, section;
		
		SensorModel::setSensor(sensor);
		
		if (!fCfg.read(filename))
		{
			ERR("Error reading file '" << filename << "' for sensor model configuration. Exiting..." << endl);
			
			exit(-1);
		}
		
		section = "sensormodel";
		
		try
		{
			key = "modelLinearVelocity";
			modelLinearVelocity = fCfg.value(section,key);
			
			key = "sigmaRho";
			sigmaRho = fCfg.value(section,key);
			
			key = "sigmaTheta";
			sigmaTheta = fCfg.value(section,key);
		}
		catch (...)
		{
			ERR("Not existing value '" << section << "/" << key << "'. Exiting..." << endl);
			
			exit(-1);
		}
	}
	
	void BasicSensorModel::likelihood(SensorReading* reading, PointIterator& particlesBegin, PointIterator& particlesEnd) const
	{
		ObjectSensorReading* osr = dynamic_cast<ObjectSensorReading*>(reading);
		
		if (osr == 0) return;
		
		BasicSensorMap* map = dynamic_cast<BasicSensorMap*>(m_sensorMap);
		
		if (map == 0) return;
		
		for (PoseParticleVector::iterator i = particlesBegin; i != particlesEnd; i++)
		{
			// We assume that the weight is resetted at the beginning of the new iteration.
			i->weight *= likelihood(map,i->pose,osr->getObservations());
		}
	}
	
	float BasicSensorModel::likelihood(BasicSensorMap* map, PointWithVelocity& pose, const vector<ObjectSensorReading::Observation>& observations) const
	{
		if (!map->isInsideWorld(pose.pose.x,pose.pose.y)) return 0.0;
		
		float deltaRho, l;
		
		l = 0.0;
		
		for (vector<ObjectSensorReading::Observation>::const_iterator it = observations.begin(); it != observations.end(); it++)
		{
			deltaRho = sqrt(((pose.pose.x - it->observation.x) * (pose.pose.x - it->observation.x)) +
							((pose.pose.y - it->observation.y) * (pose.pose.y - it->observation.y)));
			
			l += exp(-deltaRho / sigmaRho);
		}
		
		return l;
	}
	
	SENSORMODEL_FACTORY(BasicSensorModel)
}
