/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "../BasicDetector.h"
#include <opencv2/opencv.hpp>

namespace Detectors
{
	/**
	 * @class HOG
	 * 
	 * @brief Class that implements a wrapper to the OpenCV HOG detector.
	 */
	class HOG : public BasicDetector
	{
		private:
			/**
			 * @brief Detector based on the OpenCV HOG detector.
			 */
			cv::HOGDescriptor hog;
			
			/**
			 * @brief Frame of the environment.
			 */
			cv::Mat image;
			
			/**
			 * @brief number of frames per second.
			 */
			float frameRate;
			
		public:
			/**
			 * @brief Constructor that initializes the OpenCV HOG detector with the given frame rate.
			 * 
			 * @param frameRate number of frames per second.
			 */
			HOG(float frameRate);
			
			/**
			 * @brief Destructor.
			 */
			~HOG();
			
			/**
			 * @brief Function that detects object in the frame using the OpenCV HOG detector.
			 * 
			 * @param frame reference to the current frame to analyze.
			 * 
			 * @return the objects detected in the current frame.
			 */
			PTracking::ObjectSensorReading exec(const cv::Mat& frame);
			
			/**
			 * @brief Function that returns the image of the environment.
			 * 
			 * @return the image of the environment.
			 */
			inline const cv::Mat& getImage() const { return image; }
	};
}
