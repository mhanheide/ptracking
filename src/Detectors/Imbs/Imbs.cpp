/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "Imbs.h"
#include <Manfield/ConfigFile/ConfigFile.h>
#include <Manfield/Utils/DebugUtils.h>

using namespace PTracking;
using GMapping::ConfigFile;

namespace Detectors
{
	Imbs::Imbs(float frameRate) : imbs(frameRate), minArea(0), maxArea(INT_MAX), enabled(false) {;}
	
	Imbs::~Imbs() {;}
	
	ObjectSensorReading Imbs::convertDetectionsForPTracker(const VisualReading& detections) const
	{
		vector<ObjectSensorReading::Observation> observations;
		vector<VisualReading::Observation> obs;
		ObjectSensorReading visualReadings;
		
		visualReadings.setObservationsAgentPose(PTracking::Point2of(detections.getObservationsAgentPose().x,detections.getObservationsAgentPose().y,0.0));
		
		const vector<VisualReading::Observation>& obsDetection = detections.getObservations();
		
		for (vector<VisualReading::Observation>::const_iterator it = obsDetection.begin(); it != obsDetection.end(); ++it)
		{
			bool isCompletelyInside, isCompletelyOutside;
			
			isCompletelyInside = false;
			isCompletelyOutside = false;
			
			vector<VisualReading::Observation>::iterator it2 = obs.begin();
			
			for (; it2 != obs.end(); ++it2)
			{
				/// Completely inside.
				if (((it->observation.x - (it->model.width / 2)) >= (it2->observation.x - (it2->model.width / 2))) &&
					((it->observation.y - it->model.height) >= (it2->observation.y - it2->model.height)) &&
					((it->observation.x + (it->model.width / 2)) <= (it2->observation.x + (it2->model.width / 2))) &&
					(it->observation.y <= it2->observation.y))
				{
					isCompletelyInside = true;
					
					break;
				}
				
				/// Completely outside.
				if (((it->observation.x - (it->model.width / 2)) <= (it2->observation.x - (it2->model.width / 2))) &&
					((it->observation.y - it->model.height) <= (it2->observation.y - it2->model.height)) &&
					((it->observation.x + (it->model.width / 2)) >= (it2->observation.x + (it2->model.width / 2))) &&
					(it->observation.y >= it2->observation.y))
				{
					isCompletelyOutside = true;
					
					break;
				}
			}
			
			if (!isCompletelyInside)
			{
				if (isCompletelyOutside) obs.erase(it2);
				
				obs.push_back(*it);
			}
		}
		
		for (vector<VisualReading::Observation>::const_iterator it = obs.begin(); it != obs.end(); ++it)
		{
			if ((it->model.width < minWidth) || (it->model.width > maxWidth) || (it->model.height < minHeight) || (it->model.height > maxHeight)) continue;
			
			ObjectSensorReading::Observation o;
			
			o.observation.x = it->observation.x + (enabled ? topLeft.x : 0);
			o.observation.y = it->observation.y + (enabled ? topLeft.y : 0);
			
			o.head.x = it->head.x + (enabled ? topLeft.x : 0);
			o.head.y = it->head.y + (enabled ? topLeft.y : 0);
			
			o.sigma.x = it->sigma.x;
			o.sigma.y = it->sigma.y;
			
			o.model.boundingBox.first.x = it->model.boundingBox.first.x + (enabled ? topLeft.x : 0);
			o.model.boundingBox.first.y = it->model.boundingBox.first.y + (enabled ? topLeft.y : 0);
			o.model.boundingBox.second.x = it->model.boundingBox.second.x + (enabled ? topLeft.x : 0);
			o.model.boundingBox.second.y = it->model.boundingBox.second.y + (enabled ? topLeft.y : 0);
			o.model.width = it->model.width;
			o.model.height = it->model.height;
			o.model.barycenter = it->model.barycenter + (enabled ? topLeft.x : 0);
			
			for (int i = 0; i < ObjectSensorReading::Model::HISTOGRAM_VECTOR_LENGTH; ++i)
			{
				o.model.histograms[0][i] = it->model.histograms[0][i];
				o.model.histograms[1][i] = it->model.histograms[1][i];
				o.model.histograms[2][i] = it->model.histograms[2][i];
			}
			
			observations.push_back(o);
		}
		
		visualReadings.setObservations(observations);
		
		return visualReadings;
	}
	
	ObjectSensorReading Imbs::exec(const Mat& frame)
	{
		Rect roi(topLeft.x,topLeft.y,bottomRight.x,bottomRight.y);
		
		const Mat& croppedFrame = (enabled) ? frame(roi) : frame;
		
		imbs.apply(croppedFrame,foregroundImage);
		//imbs.getBackgroundImage(backgroundImage);
		
		//imshow("Foreground",foregroundImage);
		//imshow("Background",backgroundImage);
		
		return convertDetectionsForPTracker(observationManager.process(croppedFrame,foregroundImage,minArea,maxArea));
	}
	
	bool Imbs::loadBg(const string& filename)
	{
		return imbs.loadBg(filename.c_str());
	}
	
	void Imbs::saveBg(string& filename)
	{
		imbs.saveBg(&filename);
	}
	
	void Imbs::setDetectorParameters(const string& filename, int agentId, const string& calibrationDirectory)
	{
		ConfigFile fCfg;
		stringstream s;
		string key, section, temp;
		
		if (calibrationDirectory != "")
		{
			temp = calibrationDirectory;
			
			if (calibrationDirectory.at(calibrationDirectory.size() - 1) == '/') temp = calibrationDirectory.substr(0,calibrationDirectory.size() - 1);
			
			temp = temp.substr(temp.rfind("/") + 1);
		}
		
		if (!fCfg.read(filename))
		{
			ERR("Error reading file '" << filename << "' for Imbs configuration. Exiting..." << endl);
			
			exit(-1);
		}
		
		try
		{
			s << "BoundingBox_" << temp;
			
			section = s.str();
			
			key = "minArea";
			minArea = fCfg.value(section,key);
			
			key = "maxArea";
			maxArea = fCfg.value(section,key);
			
			key = "minWidth";
			minWidth = fCfg.value(section,key);
			
			key = "minHeight";
			minHeight = fCfg.value(section,key);
			
			key = "maxWidth";
			maxWidth = fCfg.value(section,key);
			
			key = "maxHeight";
			maxHeight = fCfg.value(section,key);
		}
		catch (...)
		{
			minArea = 0;
			maxArea = INT_MAX;
			minWidth = 0;
			minHeight = 0;
			maxWidth = INT_MAX;
			maxHeight = INT_MAX;
			
			s.str("");
			s.clear();
			
			s << "Not existing value '" << section << "/" << key << "' in '" << filename << "'. Setting default value for bounding box filtering.";
			
			INFO(endl);
			
			for (unsigned int i = 0; i < (s.str().size() + 4); ++i)
			{
				DEBUG("$");
			}
			
			DEBUG(endl << "$ ");
			WARN(s.str());
			DEBUG(" $" << endl);
			
			for (unsigned int i = 0; i < (s.str().size() + 4); ++i)
			{
				DEBUG("$");
			}
			
			INFO(endl);
		}
		
		try
		{
			s.str("");
			s.clear();
			
			s << "ImageCropping_Camera_" << agentId;
			
			section = s.str();
			
			key = "enabled";
			enabled = fCfg.value(section,key);
			
			key = "topLeftX";
			topLeft.x = fCfg.value(section,key);
			
			key = "topLeftY";
			topLeft.y = fCfg.value(section,key);
			
			key = "bottomRightX";
			bottomRight.x = fCfg.value(section,key);
			
			key = "bottomRightY";
			bottomRight.y = fCfg.value(section,key);
		}
		catch (...)
		{
			s.str("");
			s.clear();
			
			s << "Not existing value '" << section << "/" << key << "' in '" << filename << "'. Image cropping will be disabled.";
			
			INFO(endl);
			
			for (unsigned int i = 0; i < (s.str().size() + 4); ++i)
			{
				DEBUG("$");
			}
			
			DEBUG(endl << "$ ");
			WARN(s.str());
			DEBUG(" $" << endl);
			
			for (unsigned int i = 0; i < (s.str().size() + 4); ++i)
			{
				DEBUG("$");
			}
			
			INFO(endl);
		}
	}
}
