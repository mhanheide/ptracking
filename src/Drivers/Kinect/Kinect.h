/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "MyFreenectDevice.h"
#include "../BasicDriver.h"

namespace Drivers
{
	/**
	 * @class Kinect
	 * 
	 * @brief Class that implements a driver for the Kinect sensor.
	 */
	class Kinect : public BasicDriver
	{
		private:
			/**
			 * @brief pointer to the handler of the Kinect sensor.
			 */
			MyFreenectDevice* device;
			
			/**
			 * @brief object implementing the driver of the Kinect.
			 */
			Freenect::Freenect freenect;
			
			/**
			 * @brief depth image of the sensor.
			 */
			cv::Mat depthFrame;
			
			/**
			 * @brief identifier of the sensor opened.
			 */
			std::string serial;
			
		public:
			/**
			 * @brief Empty constructor.
			 */
			Kinect();
			
			/**
			 * @brief Destructor.
			 */
			~Kinect();
			
			/**
			 * @brief Function that reads the RGB image from the sensor.
			 * 
			 * @return the RGB image of the sensor.
			 */
			const cv::Mat& getData();
			
			/**
			 * @brief Function that reads the depth image from the sensor.
			 * 
			 * @return the depth image of the sensor.
			 */
			const cv::Mat& getDepthData();
			
			/**
			 * @brief Function that searches for all connected Kinect.
			 */
			void searchSerialDevices();
	};
}
