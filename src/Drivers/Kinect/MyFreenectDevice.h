/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <opencv2/core/core.hpp>
#include <libfreenect.hpp>

static const int FREENECT_FRAME_WIDTH		= 640;
static const int FREENECT_FRAME_HEIGHT		= 480;
static const int FREENECT_FRAME_PIX			= FREENECT_FRAME_WIDTH * FREENECT_FRAME_HEIGHT;
static const int FREENECT_VIDEO_RGB_SIZE	= FREENECT_FRAME_PIX * 3;

namespace Drivers
{
	/**
	 * @class Mutex
	 * 
	 * @brief Class that uses a scoped mutex.
	 */
	class Mutex
	{
		private:
			/**
			 * @brief mutex to handle the writing/reading of either a depth or a image.
			 */
			pthread_mutex_t mutex;
			
		public:
			/**
			 * @brief Empty constructor.
			 * 
			 * It initializes the mutex.
			 */
			Mutex()
			{
				pthread_mutex_init(&mutex,0);
			}
			
			/**
			 * @brief Function that locks the mutex.
			 */
			inline void lock()
			{
				pthread_mutex_lock(&mutex);
			}
			
			/**
			 * @brief Function that unlocks the mutex.
			 */
			inline void unlock()
			{
				pthread_mutex_unlock(&mutex);
			}
			
			/**
			 * @class ScopedLock
			 * 
			 * @brief Class that implements a scoped mutex.
			 */
			class ScopedLock
			{
				/**
				 * @brief reference to a mutex.
				 */
				Mutex& mutex;
				
				public:
					/**
					 * @brief Constructor that initializes a mutex with the one given as input.
					 * 
					 * @param mutex reference to the mutex to be initialized.
					 */
					ScopedLock(Mutex& mutex) : mutex(mutex) {;}
					
					/**
					 * @brief Function that locks the mutex.
					 */
					inline void lock()
					{
						mutex.lock();
					}
					
					/**
					 * @brief Destructor.
					 * 
					 * It unlocks the mutex.
					 */
					~ScopedLock()
					{
						mutex.unlock();
					}
			};
	};
	
	/**
	 * @class MyFreenectDevice
	 * 
	 * @brief Class that uses the freenect driver to read data from the sensor.
	 */
	class MyFreenectDevice : public Freenect::FreenectDevice
	{
		private:
			/**
			 * @brief depth image of the sensor.
			 */
			cv::Mat depthFrame;
			
			/**
			 * @brief RGB image of the sensor.
			 */
			cv::Mat frame;
			
			/**
			 * @brief mutex for managing the reading/writing of the depth image.
			 */
			Mutex depthMutex;
			
			/**
			 * @brief mutex for managing the reading/writing of the RGB image.
			 */
			Mutex rgbMutex;
			
			/**
			 * @brief Function that is a callback where the depth image is read.
			 * 
			 * @param depth pointer to the depth image.
			 * @param timestamp timestamp where the depth image has been read.
			 */
			void DepthCallback(void* depth, uint32_t timestamp);
			
			/**
			 * @brief Function that is a callback where the RGB image is read.
			 * 
			 * @param rgb pointer to the RGB image.
			 * @param timestamp timestamp where the RGB image has been read.
			 */
			void VideoCallback(void* rgb, uint32_t timestamp);
			
		public:
			/**
			 * @brief Constructor that initializes the freenect driver using the parameters given as input.
			 * 
			 * @param context pointer to the sensor opened.
			 * @param serial identifier of the sensor opened.
			 */
			MyFreenectDevice(freenect_context* context, const std::string& serial);
			
			/**
			 * @brief Function that returns the depth image.
			 * 
			 * @return the depth image.
			 */
			const cv::Mat& getDepthData();
			
			/**
			 * @brief Function that returns the RGB image.
			 * 
			 * @return the RGB image.
			 */
			const cv::Mat& getData();
	};
}
