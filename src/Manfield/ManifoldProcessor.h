/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include "SensorFilter.h"
#include <vector>

namespace manfield
{
	class ManifoldFilterProcessor
	{
		protected:
			typedef std::map<std::string, SensorFilter*> FilterBank;
			
			FilterBank m_filterBank;
			PTracking::PointWithVelocity m_lastOdometryPose;
			float m_timeOfLastIteration;
			bool m_bootstrapRequired, m_first, ownFilterBank;
			
			virtual void singleFilterIteration(SensorFilter* sf, const PTracking::PointWithVelocity& odometry,
											   const std::vector<GMapping::SensorReading*>& reading);
			
		public:
			ManifoldFilterProcessor (bool ownFB = false);
			virtual ~ManifoldFilterProcessor();
			
			virtual void addSensorFilter(SensorFilter* filter);
			const FilterBank& getFilterBank() const { return m_filterBank; }
			void init();
			void initFromReadings(const std::vector<GMapping::SensorReading*>& reading);
			void initFromUniform();
			virtual void processReading(const PTracking::PointWithVelocity& odometry, const std::vector<GMapping::SensorReading*>& reading);
			virtual void updateBootStrap();
			virtual bool updateNeeded() { return true; }
	};
}
