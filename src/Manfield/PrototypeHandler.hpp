/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include <exception>

template<class P> std::map<std::string, const P*>& PrototypeFactory<P>::getPrototypeDb()
{
	static std::map<std::string, const P*> db;
	
	return db;
}

template<class P> PrototypeFactory<P>::PrototypeFactory(const P* prototype) throw (std::runtime_error)
{
	std::map<std::string, const P*>& prototypeDb = PrototypeFactory<P>::getPrototypeDb();
	std::string name = prototype->toString();
	
	typename std::map<std::string, const P*>::const_iterator it = prototypeDb.find(name);
	
	if (it !=  prototypeDb.end()) throw std::runtime_error("Prototype " + name + " already present in database.");
	
	P* clone = prototype->clone();
	
	if (!clone) throw std::runtime_error("Prototype " + name + " does not support clone().\n" + "Not added to database.");
	
	std::string name2 = clone->toString();
	
	if (name2 != name) throw std::runtime_error("Converting class '" + name + "' to string returns '" + name2 + "'. \nNot added to database.");
	
	delete clone;
	
	prototypeDb[name] = prototype;
}

template<class P> P* PrototypeFactory<P>::forName(const std::string& name) throw (std::runtime_error)
{
	std::map<std::string, const P*>& prototypeDb = PrototypeFactory<P>::getPrototypeDb();
	
	typename std::map<std::string, const P*>::iterator i = prototypeDb.find(name);
	
	if ( i == prototypeDb.end()) throw std::runtime_error("Prototype '" + name + "' is not present in database.");
	
	P* clone = i->second->clone();
	
	if (!clone) throw std::runtime_error("Could not clone prototype '" + name + "'.");
	
	return clone;
}

template<class P> void PrototypeFactory<P>::freePrototypeDb()
{
	std::map<std::string, const P*>& prototypeDb = PrototypeFactory<P>::getPrototypeDb();
	
	for (typename std::map<std::string, const P*>::iterator it = prototypeDb.begin(); it != prototypeDb.end(); ++it)
	{
		delete it->second;
	}
	
	prototypeDb.clear();
}
