/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <Detectors/BasicDetector.h>
#include <Drivers/BasicDriver.h>
#include <PTracker/PTracker.h>
#include <ThirdParty/Gnuplot/GnuplotGUI.h>

/**
 * @class PTaLer
 * 
 * @brief Class that implements a distributed tracker using a distributed particle filtering method.
 */
class PTaLer
{
	private:
		/**
		 * @brief maximum number of missing frames before exiting.
		 */
		static const int MAXIMUM_NUMBER_OF_MISSING_FRAMES = 200;
		
		/**
		 * @brief color map to draw the PTracker estimations.
		 */
		std::map<int,std::pair<int,std::pair<int,int> > > colorMap;
		
		/**
		 * @brief Positions of the Gnuplot GUIs on the screen.
		 */
		std::map<int,std::pair<int,int> > screenPositionGnuplotGUIs;
		
		/**
		 * @brief distributed tracker that uses a distributed particle filtering method.
		 */
		PTracker pTracker;
		
		/**
		 * @brief type of the detector used to identify moving objects.
		 */
		std::string detectorType;

		/**
		 * @brief directory containing the calibration files.
		 */
		std::string calibrationDirectory;

		/**
		 * @brief number of frames per second.
		 */
		float frameRate;
		
		/**
		 * @brief id of the agent.
		 */
		int agentId;
		
		/**
		 * @brief height of the screen.
		 */
		int screenHeight;
		
		/**
		 * @brief width of the screen.
		 */
		int screenWidth;
		
		/**
		 * @brief Function that assigns a position on the screen to the Gnuplot window of the current estimation.
		 * 
		 * @param estimationIdentity estimation to be assigned.
		 * 
		 * @return a position on the screen to the Gnuplot window of the current estimation.
		 */
		std::pair<int,int> assignScreenPositionGnuplotWindow(int estimationIdentity);
		
		/**
		 * @brief Function that draws the estimations on the current frame.
		 * 
		 * @param frame reference to the current frame.
		 * @param estimations reference to the estimations provided by PTracker.
		 * @param fps current frame per seconds.
		 * @param frameRateCounter current number of frames elaborated.
		 * @param planView reference to the plan view frame.
		 * @param globalEstimations reference to the global estimations provided by PTracker.
		 */
		void draw(const cv::Mat& frame, const PTracking::SingleAgentEstimations& estimations, float fps, const std::string& frameRateCounter, const cv::Mat& planView = cv::Mat(),
				  const PTracking::MultiAgentEstimations& globalEstimations = PTracking::MultiAgentEstimations()) const;
		
		/**
		 * @brief Function that instantiates a detector with a given frame rate.
		 * 
		 * @param frameRate number of frames per second.
		 * 
		 * @return a pointer to the chosen detector.
		 */
		Detectors::BasicDetector* instantiateDetector(float frameRate) const;
		
		/**
		 * @brief Function that instantiates a driver for the sensor given in input.
		 * 
		 * @param sensorType type of sensor.
		 * 
		 * @return a pointer to the chosen driver.
		 */
		Drivers::BasicDriver* instantiateDriver(const std::string& sensorType) const;
		
		/**
		 * @brief Function that reads a new image from file.
		 * 
		 * @param filename name of the image to read (if isLiveSensor is false).
		 * @param isLiveSensor \b true means that data are read from a sensor like Kinect or Xtion, \b false means that data are read from images.
		 * @param driver pointer to the current driver.
		 * 
		 * @return the image read, if any.
		 */
		std::pair<std::string,cv::Mat> readFrame(const std::string& filename, bool isLiveSensor = false, Drivers::BasicDriver* driver = 0);
		
		/**
		 * @brief Function that updates the history of the estimations provided by PTracker.
		 * 
		 * @param estimations reference to the current estimations performed by PTracker.
		 */
		void updateEstimationHistory(const PTracking::MultiAgentEstimations& estimations);
		
	public:
		/**
		 * @brief Constructor that takes the several parameters as initialization value.
		 * 
		 * It initializes frameRate, agent id, filename and calibrationDirectory with the ones given as input.
		 * 
		 * @param frameRate number of frames per second.
		 * @param agentId id of the agent.
		 * @param filename file to be read.
		 * @param calibrationDirectory directory where the calibration data have to be read.
		 */
		PTaLer(float frameRate, int agentId = -1, const std::string& filename = "", const std::string& calibrationDirectory = "");
		
		/**
		 * @brief Destructor.
		 */
		~PTaLer();
		
		/**
		 * @brief Function that executes tracking using data coming from a detector, and when needed it performs the learning to generate the IRL model.
		 * 
		 * @param problemFile file containing the linear programming problem.
		 * @param filename path where data have to be found.
		 * @param backgroundFilename file containing the background data.
		 * @param isLiveSensor \b true means that data are read from a sensor like Kinect or Xtion, \b false means that data are read from images.
		 * @param isPause \b true means that a key must be pressed in order to start the tracking, \b false there is no such a need.
		 */
		void exec(const std::string& problemFile, const std::string& filename, const std::string& backgroundFilename = "", bool isLiveSensor = false, bool isPause = false);
		
		/**
		 * @brief Function that first executes tracking reading data from a file and then it performs the learning to generate the IRL model.
		 * 
		 * @param problemFile file containing the linear programming problem.
		 * @param observationFile file containing the trajectories estimated by PTracking.
		 */
		void execDataset(const std::string& problemFile, const std::string& observationFile);
};
