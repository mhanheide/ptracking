/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#include "PTaLer.h"

static const string USAGE = "Usage: ./PTaLer <problem-file> <frame-rate> -img <first-image-file> [ -loadbg <background-file> -calib <calibration-directory> -agentId <agent-number> -pause ] OR\n"
							"       ./PTaLer <problem-file> <frame-rate> -s <sensor-type> [ -loadbg <background-file> -calib <calibration-directory> -agentId <agent-number> -pause ] OR\n"
							"       ./PTaLer <problem-file> <frame-rate> -obs <observation-file>.";

bool isNumber(const string& parameter)
{
	stringstream s;
	int temp;
	
	if (parameter == "0") return true;
	
	s << parameter;
	
	s >> temp;
	
	if (temp == 0) return false;
	else return true;
}

int main(int argc, char** argv)
{
	string backgroundFilename, calibrationDirectory, firstImage, observationFile, parameterFilename, sensorType;
	float frameRate;
	int agentId;
	bool isPause;
	
	if ((argc < 5) || (argc > 12))
	{
		ERR(USAGE << endl);
		
		exit(-1);
	}
	
	if (!isNumber(argv[2]))
	{
		WARN("Frame rate has to be a number." << endl << endl);
		ERR(USAGE << endl);
		
		exit(-1);
	}
	
	frameRate = atof(argv[2]);
	
	if (frameRate <= 0)
	{
		ERR("Frame rate has to be greater than 0." << endl);
		
		exit(-1);
	}
	
	if (string(argv[3]) == "-img") firstImage = argv[4];
	else if (string(argv[3]) == "-obs") observationFile = argv[4];
	else if (string(argv[3]) == "-s") sensorType = argv[4];
	else
	{
		ERR(USAGE << endl);
		
		exit(-1);
	}
	
	backgroundFilename = "";
	calibrationDirectory = "";
	agentId = -1;
	isPause = false;
	
	if ((argc > 5) && (string(argv[3]) == "-img") || (string(argv[3]) == "-s"))
	{
		for (int i = 5; i < argc; i += 2)
		{
			if (argv[i] == 0) continue;
			
			if ((string(argv[i]) == "-loadbg") && (argv[i + 1] != 0)) backgroundFilename = argv[i + 1];
			else if ((string(argv[i]) == "-calib") && (argv[i + 1] != 0)) calibrationDirectory = argv[i + 1];
			else if ((string(argv[i]) == "-agentId") && (argv[i + 1] != 0))
			{
				if (!isNumber(argv[i + 1]))
				{
					WARN("Agent id has to be a number." << endl << endl);
					ERR(USAGE << endl);
					
					exit(-1);
				}
				
				agentId = atoi(argv[i + 1]);
			}
			else if (string(argv[i]) == "-pause") isPause = true;
			else
			{
				ERR(USAGE << endl);
				
				exit(-1);
			}
		}
	}
	else if (argc > 5)
	{
		ERR(USAGE << endl);
		
		exit(-1);
	}
	
	parameterFilename = "";
	
	if (calibrationDirectory != "") parameterFilename = calibrationDirectory + string("/parameters.cfg");
	
	PTaLer pTaler(frameRate,agentId,parameterFilename,calibrationDirectory);
	
	if (string(argv[3]) == "-img") pTaler.exec(argv[1],firstImage,backgroundFilename,false,isPause);
	else if (string(argv[3]) == "-s") pTaler.exec(argv[1],sensorType,backgroundFilename,true,isPause);
	else pTaler.execDataset(argv[1],observationFile);
	
	exit(0);
}
