/*
 * PTracking - Distributed real-time multiple object tracking library.
 * Copyright (c) 2014, Fabio Previtali. All rights reserved.
 * 
 * This file is part of PTracking.
 * 
 * PTracking is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * PTracking is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License
 * along with PTracking. If not, see <http://www.gnu.org/licenses/>.
 * 
 * Written by Fabio Previtali.
 * 
 * Please, report any suggestion/comment/bug to fabio.previtali@gmail.com.
 */

#pragma once

#include <sys/socket.h>
#include <cstring>

namespace PTracking
{
	/**
	 * @class SocketAddress
	 * 
	 * @brief Class that redefines the \a sockaddr structure.
	 */
	class SocketAddress
	{
		protected:
			/**
			 * @brief address of the sender/receiver.
			 */
			struct sockaddr address;
			
		public:
			/**
			 * @brief Empty constructor.
			 * 
			 * It initializes the address with the default value.
			 */
			SocketAddress() { bzero(&address,sizeof(address)); }
			
			/**
			 * @brief Constructor that takes an address as initialization value.
			 * 
			 * It initializes the address with the one given as input.
			 * 
			 * @param address new address to be set.
			 */
			inline SocketAddress(const struct sockaddr& address) : address(address) {;}
			
			/**
			 * @brief Function that returns the address of the sender/receiver.
			 * 
			 * @return the address of the sender/receiver.
			 */
			inline struct sockaddr getAddress() { return address; }
			
			/**
			 * @brief Function that returns the family of the sender/receiver address.
			 * 
			 * @return the family of the sender/receiver address.
			 */
			inline short getFamily() const { return address.sa_family; }
	};
}
