#pragma once

namespace PTracking
{
	/**
	 * @brief human-readable typedef of the estimations performed by the team of agents.
	 */
	typedef std::map<int,std::pair<std::pair<PTracking::ObjectSensorReading::Observation,PTracking::Point2f>,std::pair<std::string,int> > > MultiAgentEstimations;
	
	/**
	 * @brief human-readable typedef of the estimations performed by the agent.
	 */
	typedef std::map<int,std::pair<PTracking::ObjectSensorReading::Observation,PTracking::Point2f> > SingleAgentEstimations;
}
