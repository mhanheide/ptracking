# - Try to find Imbs include dirs and libraries
#	
#	Usage of this module as follows:
#	
#	  find_package(Imbs)
#	
#	Variables defined by this module:
#	
#	  Imbs_FOUND			System has Imbs, include and library dirs found
#	  Imbs_INCLUDE_DIR		The Imbs include directories.
#	  Imbs_LIBRARY			The Imbs library

find_path(/usr/local/include NAMES Imbs/Core/Imbs.hpp)
set(Imbs_ROOT_DIR /usr/local/include/Imbs)
find_path(Imbs_INCLUDE_DIR NAMES Imbs/Core/Imbs.hpp HINTS ${Imbs_ROOT_DIR})

find_library(Imbs_LIBRARY NAMES imbs HINTS ${Imbs_ROOT_DIR}/../../lib/Imbs)

if (Imbs_LIBRARY STREQUAL "Imbs_LIBRARY-NOTFOUND")
	if (Imbs_FIND_REQUIRED)
		message(FATAL_ERROR "Imbs library has NOT been found. Did you install the library?")
	endif (Imbs_FIND_REQUIRED)
	
	set(Imbs_FOUND 0)
else (Imbs_LIBRARY STREQUAL "Imbs_LIBRARY-NOTFOUND")
	message(STATUS "Found Imbs")
	set(Imbs_FOUND 1)
endif (Imbs_LIBRARY STREQUAL "Imbs_LIBRARY-NOTFOUND")
